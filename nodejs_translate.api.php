<?php

/**
 * @file
 * Hooks provided by the Node.js Translate module.
 */

/**
 * Provides the ability to match site Languages and ISO 639-1 codes.
 *
 * @param array $languages
 *   Match Drupal langcodes => ISO 639-1 codes.
 */
function hook_nodejs_translate_languages_alter(array &$languages) {
  // @see https://cloud.google.com/translate/docs/languages for ISO-639 codes.
  $languages['zh-hans'] = 'zh';
  $languages['pt-pt'] = 'pt';
  $languages['pt-br'] = 'pt';
}

/**
 * Allows to translate additional fields.
 *
 * Implements hook_nodejs_translate_entity_alter().
 *
 * @param object $entity_translated
 *   New entity translation.
 * @param object $entity_original
 *   Default language, original entity translation.
 * @param string $source_language
 *   ISO-639 langcode.
 * @param string $destination_language
 *   ISO-639 langcode.
 */
function hook_nodejs_translate_entity_alter(&$entity_translated, $entity_original, $source_language, $destination_language) {
  if ($entity_translated->hasField('field_text')) {
    $text_translated = \Drupal::service('nodejs_translate.nodejs_translator')->translateText($entity_original->field_text->value, $source_language, $destination_language);
    $entity_translated->field_text->value = $text_translated;
    $entity_translated->field_text->format = $entity_original->field_text->format;
  }
}
