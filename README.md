# Node.js Translate

Node.JS Translate module consumes Google Translate for free.

Node.js Translate module allows to get translation automatically
for text or HTML in many languages.
Node.js Translate works together with this library:
https://www.npmjs.com/package/@iamtraction/google-translate

It ~abuses~ uses Google Translate page via Node.js and
returns translation back to Drupal.
It doesn't need any Google API keys and it's FREE.
Obviously, it's against Google policy.
But you can use this module for learning Drupal and
Node.js or create own pet projects.

## Installation

1. Install as you would normally install a contributed Drupal module.
   For further information, see
   [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

2. You also need to install Node.js:
   https://nodejs.org/en

3. And run these commands in libraries/google-translate folder:
```
npm install
npm start
```

4. Node.js Translate service will work only if your terminal is working.
   If you need to run it automatically, use PM2 to run process in background
   and startup it automatically.

```
npm install pm2@latest -g
yarn global add pm2
pm2 start index.js
pm2 save
```

## Configuration

Before start translation, you need to run node.js service
on your local server or use external server.
If you use external server, then you will need
to update IP address on configuration form:
/admin/config/regional/nodejs_translate

Also, increase delay time between requests for long texts,
and decrease delay time for short texts like taxonomy term names.

## Usage

Node.js Translate provides single and multiple translation
drush commands:

### Single Translate

Single translate command can translate only one entity.
You also need to pass Entity Type ID as first argument.

nodejs_translate:single_translate <entity_type_id> <entity_id>

**entity_type_id** - node, block_content, taxonomy_term, etc.
**entity_id** - nid, block id, tid, etc.

Usage:
```
drush nodejs_translate:single_translate node 42
```

aliases nodejs-st
```
drush nodejs-st node 42
```

### Multiple Translate

Multiple translate command can translate all entities
for one bundle (node type, taxonomy vocabulary, block type).
You need to pass Entity Type ID and Bundle in drush command.

nodejs_translate:multiple_translate <entity_type_id> <bundle>

Usage:
```
drush nodejs_translate:single_translate node article
```

aliases nodejs-mt
```
drush nodejs-mt node article
```

## Batch for node types

You can try to translate nodes in Batch
for selected node type:
/admin/content/translate-nodes

It's possible for small pages with small amount of text.
For long pages use drush commands for single or multiple translate.

## Node.js Translator service

Use getTranslation() method to translate short stings or texts:
```
\Drupal::service('nodejs_translate.nodejs_translator')->getTranslation($string, $from, $to)
```

Use translateText() method to translate texts longer than 4000 characters:
```
\Drupal::service('nodejs_translate.nodejs_translator')->translateText($text, $from, $to)
```

## Limitations

You can run translate for big texts and need to split HTML/text in chunks
less than 4900 characters. And you need to set up delay between
each translation. Delays must be different, it can't be the same,
because we need to pretend real user behaviour. Node.js Translate has
configuration for delay values.
Also, I couldn't find smart split on chunks to support right splitting
without cutting HTML tags in the middle. Feel free to suggest smart split
library in the module issues.

## Disclaimer

The only way to translate a site is through Google Translate.
Before, the Google TOS only allowed to access the service through
the interface provided by Google.
https://policies.google.com/terms/archive/20070416-20120301?gl=US&hl=en

See point 5.3:

You agree not to access (or attempt to access) any of
the Services by any means other than through the interface
that is provided by Google, unless you have been specifically
allowed to do so in a separate agreement with Google.
You specifically agree not to access (or attempt to access)
any of the Services through any automated means
(including use of scripts or web crawlers) and
shall ensure that you comply with the instructions
set out in any robots.txt file present on the Services.

In short, it's against Google policy, don't use it in real projects.
