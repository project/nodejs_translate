<?php

namespace Drupal\nodejs_translate\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\nodejs_translate\NodejsTranslator;
use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\nodejs_translate\Commands
 */
class MultipleTranslateCommand extends DrushCommands {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Translator for text service.
   *
   * @var \Drupal\nodejs_translate\NodejsTranslator
   */
  protected $translator;

  /**
   * Constructs Node.js Multiple Translate command.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\nodejs_translate\NodejsTranslator $translator
   *   The text translator.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, NodejsTranslator $translator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->translator = $translator;
  }

  /**
   * Drush command that translate.
   *
   * @param string $entity_type_id
   *   Entity Type ID.
   * @param string $bundle
   *   Bundle of entity type.
   *
   * @command nodejs_translate:multiple_translate entity_type_id bundle
   * @aliases nodejs-mt
   * @usage nodejs_translate:multiple_translate node article
   */
  public function message($entity_type_id, $bundle) {
    if (empty($entity_type_id) || empty($bundle)) {
      $this->output()->writeln('nodejs_translate:multiple_translate command requires Entity Type ID and Bundle arguments');
    }

    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    if (empty($storage)) {
      $this->output()->writeln('Couldn\'t load storage for entity type: ' . $entity_type_id);
      return;
    }

    $query = $storage->getQuery();
    switch ($entity_type_id) {
      case 'menu_link_content':
        $query = $query->condition('menu_name', $bundle);
        break;

      case 'node':
        $query = $query->condition('type', $bundle);
        break;

      default:
        $query = $query->condition('bundle', $bundle);

    }

    $total = $query->accessCheck(FALSE)
      ->count()
      ->execute();

    if (empty($total) || $total < 1) {
      $this->output()->writeln('Missing entities to translate.');
      return;
    }

    $query = $storage->getQuery();
    $counter = 0;
    while ($counter < $total) {
      switch ($entity_type_id) {
        case 'menu_link_content':
          $query = $query->condition('menu_name', $bundle);
          break;

        case 'node':
          $query = $query->condition('type', $bundle);
          break;

        default:
          $query = $query->condition('bundle', $bundle);

      }
      $ids = $query->accessCheck(FALSE)
        ->range($counter, 10)
        ->execute();
      $entities = $storage->loadMultiple($ids);

      if (empty($entities)) {
        break;
      }

      foreach ($entities as $entity) {
        $counter++;
        $this->translator->translateEntity($entity);
        $this->output()->writeln($counter . ' of ' . $total .
          ' has been translated: ' . $entity_type_id . ' - ' . $entity->label());
      }
    }

  }

  /**
   * Gets a list of content entities.
   *
   * @return array
   *   An array of metadata about content entities.
   */
  protected function getBundleableEntitiesList() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $content_entities = [];
    foreach ($entity_types as $key => $entity_type) {
      if ($entity_type->getBundleEntityType() && ($entity_type->get('field_ui_base_route') != '')) {
        $content_entities[$key] = [
          'content_entity' => $key,
          'content_entity_bundle' => $entity_type->getBundleEntityType(),
        ];
      }
    }
    return $content_entities;
  }

}
