<?php

namespace Drupal\nodejs_translate\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\nodejs_translate\NodejsTranslator;
use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\nodejs_translate\Commands
 */
class SingleTranslateCommand extends DrushCommands {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Translator for text service.
   *
   * @var \Drupal\nodejs_translate\NodejsTranslator
   */
  protected $translator;

  /**
   * Constructs Node.js Single Translate command.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\nodejs_translate\NodejsTranslator $translator
   *   The text translator.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, NodejsTranslator $translator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->translator = $translator;
  }

  /**
   * Drush command that translate.
   *
   * @param string $entity_type_id
   *   Entity Type ID.
   * @param string $id
   *   Argument with node ID.
   *
   * @command nodejs_translate:single_translate entity_type_id entity_id
   * @aliases nodejs-st
   * @usage nodejs_translate:single_translate node 42
   */
  public function message($entity_type_id, $id) {
    if (empty($entity_type_id) || empty($id)) {
      $this->output()->writeln('nodejs_translate:single_translate command requires Entity Type ID and Entity ID arguments');
    }
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    if (empty($storage)) {
      $this->output()->writeln('Couldn\'t load storage for entity type: ' . $entity_type_id);
      return;
    }

    $entity = $storage->load($id);
    if (empty($entity)) {
      $this->output()->writeln('Missing entity with ID: ' . $id);
      return;
    }

    $this->translator->translateEntity($entity);
    $this->output()->writeln('Node has been translated: /node/' . $id);
  }

}
