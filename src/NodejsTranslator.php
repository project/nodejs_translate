<?php

namespace Drupal\nodejs_translate;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Node.js Translator for text.
 */
class NodejsTranslator {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Config Factory Service Object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new TranslateText object.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   A Guzzle client object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(ClientFactory $http_client_factory, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {
    $this->httpClientFactory = $http_client_factory;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Translate text.
   *
   * @param string $text
   *   Text for translation.
   * @param string $from
   *   Name/ISO 639-1 code, for example 'en'.
   * @param string $to
   *   Name/ISO 639-1 code, for example 'es'.
   *
   * @return string
   *   Translated text.
   */
  public function translateText($text, $from, $to) {
    $config = $this->configFactory->get('nodejs_translate.settings');
    if (strlen($text) > 4900) {
      $chunks = $this->breakLongText($text);
    }
    else {
      $chunks = [$text];
    }

    $translated_text = '';
    foreach ($chunks as $chunk) {
      $translated_text .= $this->getTranslation($chunk, $from, $to);
      $delay_from = $config->get('delay_from') ?? 5;
      $delay_to = $config->get('delay_to') ?? 10;
      $random_time = rand($delay_from, $delay_to);
      sleep($random_time);
    }

    return $translated_text;
  }

  /**
   * Translate helper function.
   *
   * @param string $text
   *   Languages: ru, es, en.
   * @param string $from
   *   Name/ISO 639-1 code, for example 'en'.
   * @param string $to
   *   Name/ISO 639-1 code, for example 'es'.
   *
   * @return string
   *   Translated string
   */
  public function getTranslation($text, $from, $to) {
    $client = \Drupal::service('http_client_factory')->fromOptions();

    $request = $client->post($this->getHost(), [
      'form_params' => [
        'translateText' => $text,
        'translateFrom' => $from,
        'translateTo' => $to,
      ],
    ]);
    $response = $request->getBody();
    $contents = $response->getContents();
    if (empty($contents) || $request->getStatusCode() != '200') {
      return FALSE;
    }

    return $contents;
  }

  public function getHost() {
    $config = $this->configFactory->get('nodejs_translate.settings');
    $host = $config->get('nodejs_host') ?? 'http://localhost:8000';
    $additional_hosts = $config->get('additional_nodejs_hosts');
    if (!empty($additional_hosts)) {
      $additional_hosts = explode(PHP_EOL, $additional_hosts);
      $hosts = [];
      $hosts[] = $host;
      foreach ($additional_hosts as $additional_host) {
        if (!empty($additional_host)) {
          $hosts[] = $additional_host;
        }
      }
    }

    if (isset($hosts) && count($hosts) > 1) {
      $last_host = \Drupal::state()->get('nodejs_host');
      if (in_array($last_host, $hosts)) {
        $key = array_search($last_host, $hosts);
        if ($key == count($hosts) - 1) {
          $host = $hosts[0];
        }
        else {
          $host = $hosts[$key + 1];
        }
      }
      \Drupal::state()->set('nodejs_host', $host);
    }


    return $host;
  }

  /**
   * Break long text into chunks.
   */
  public function breakLongText($text, $length = 4000, $maxLength = 4500) {
    // Text length.
    $textLength = strlen($text);

    // Initialize empty array to store split text.
    $splitText = [];

    // Return without breaking if text is already short.
    if (!($textLength > $maxLength)) {
      $splitText[] = $text;
      return $splitText;
    }

    // Guess sentence completion.
    $needle = '</div>';

    // Iterate over $text length as substr_replace deleting it.
    while (strlen($text) > $length) {
      $end = strpos($text, $needle, $length);
      if ($end === FALSE) {
        // Returns FALSE if the needle (in this case ".") was not found.
        $splitText[] = substr($text, 0);
        $text = '';
        break;
      }

      $end = $end + 6;
      $splitText[] = substr($text, 0, $end);
      $text = substr_replace($text, '', 0, $end);
    }

    if ($text) {
      $splitText[] = substr($text, 0);
    }

    return $splitText;
  }

  /**
   * Get site langcodes and match them with ISO 639-1 codes.
   */
  public function getLanguages() {
    $langcodes = $this->languageManager->getLanguages();
    $languages = [];
    foreach ($langcodes as $langcode => $language) {
      $languages[$langcode] = $langcode;
    }
    $this->moduleHandler->alter('nodejs_translate_languages', $languages);
    // Array in format 'Drupal langcode' => 'ISO 639-1 code'.
    return $languages;
  }

  /**
   * Translate any entity.
   */
  public function translateEntity($entity) {
    $default_langcode = $entity->getUntranslated()->language()->getId();
    $languages = $this->getLanguages();
    $source_language = $languages[$default_langcode];
    unset($languages[$default_langcode]);

    foreach ($languages as $langcode => $destination_language) {
      $translation_status = $entity->hasTranslation($langcode);
      if (!empty($translation_status)) {
        continue;
      }

      $entity_translated = $entity->addTranslation($langcode);
      $title_translated = $this->translateText($entity->label(), $source_language, $destination_language);
      if ($entity->hasField('title')) {
        if (method_exists($entity_translated, 'setTitle')) {
          $entity_translated->setTitle($title_translated);
        }
        else {
          $entity_translated->title = $title_translated;
        }
      }
      if ($entity->hasField('info')) {
        if (method_exists($entity_translated, 'setInfo')) {
          $entity_translated->setInfo($title_translated);
        }
        else {
          $entity_translated->info = $title_translated;
        }
      }

      if ($entity->hasField('name')) {
        if (method_exists($entity_translated, 'setName')) {
          $entity_translated->setName($title_translated);
        }
        else {
          $entity_translated->name = $title_translated;
        }
      }

      if ($entity->hasField('uid')) {
        $entity_translated->uid = $entity->get('uid')->getValue();
      }

      if ($entity->hasField('body')) {
        $body_translated = $this->translateText($entity->body->value, $source_language, $destination_language);
        $entity_translated->body->value = $body_translated;
        $entity_translated->body->format = $entity->body->format;
      }

      if ($entity->hasField('description') &&
        !$entity->description->isEmpty() &&
        !empty($entity->description->value)) {
        $entity_translated->description->value = $this->translateText($entity->description->value, $source_language, $destination_language);
        $entity_translated->description->format = $entity->description->format;
      }

      // Add another fields for translation.
      $this->moduleHandler->alter('nodejs_translate_entity', $entity_translated, $entity, $source_language, $destination_language);
      $entity_translated->save();
    }
  }

}
