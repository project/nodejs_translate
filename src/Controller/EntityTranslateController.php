<?php

namespace Drupal\nodejs_translate\Controller;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\nodejs_translate\NodejsTranslator;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Node.js translation controller.
 */
class EntityTranslateController extends ControllerBase {

  /**
   * Translator for text service.
   *
   * @var \Drupal\nodejs_translate\NodejsTranslator
   */
  protected $translator;

  /**
   * BookTranslateController constructor.
   *
   * @param \Drupal\nodejs_translate\NodejsTranslator $translator
   *   The text translator.
   */
  public function __construct(NodejsTranslator $translator) {
    $this->translator = $translator;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nodejs_translate.nodejs_translator')
    );
  }

  /**
   * Translate single node.
   */
  public function singleTranslateNode(Node $node, Request $request) {
    $this->translator->translateEntity($node);
    $text = $this->t('Translated:') . ' ' . $node->label() . '.<br>';
    $text .= $node->toLink()->toString();
    return [
      '#type' => 'markup',
      '#markup' => $text,
    ];
  }

  /**
   * Translate single block.
   */
  public function singleTranslateBlock(BlockContent $block_content, Request $request) {
    $this->translator->translateEntity($block_content);
    $text = $this->t('Translated:') . ' ' . $block_content->label() . '.<br>';
    $text .= $block_content->toLink()->toString();
    return [
      '#type' => 'markup',
      '#markup' => $text,
    ];
  }

  /**
   * Translate single term.
   */
  public function singleTranslateTerm(Term $taxonomy_term, Request $request) {
    $this->translator->translateEntity($taxonomy_term);
    $text = $this->t('Translated:') . ' ' . $taxonomy_term->label() . '.<br>';
    $text .= $taxonomy_term->toLink()->toString();
    return [
      '#type' => 'markup',
      '#markup' => $text,
    ];
  }

}
