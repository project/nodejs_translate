<?php

namespace Drupal\nodejs_translate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Node.js Translate settings.
 */
class NodejsTranslateSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodejs_translate_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nodejs_translate.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nodejs_translate.settings');

    $form['delay_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delay from'),
      '#default_value' => $config->get('delay_from') ?? 5,
      '#description' => $this->t('Delay in seconds'),
    ];

    $form['delay_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delay to'),
      '#default_value' => $config->get('delay_to') ?? 10,
      '#description' => $this->t('Delay in seconds'),
    ];

    $form['nodejs_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node.js service host'),
      '#default_value' => $config->get('nodejs_host') ?? 'http://localhost:8000',
      '#description' => $this->t('For example http://localhost:8000 or http://remote_ip_address:port'),
    ];
    $form['additional_nodejs_hosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Node.js service host'),
      '#default_value' => $config->get('additional_nodejs_hosts') ?? '',
      '#description' => $this->t('Server per line, for example http://111.22.33.44 or http://domain.name:port. You need to set up proxy with Nginx/Apache to localhost:8000 or change domain in index.js to specify URL with post'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('nodejs_translate.settings')
      ->set('delay_from', $form_state->getValue('delay_from'))
      ->set('delay_to', $form_state->getValue('delay_to'))
      ->set('nodejs_host', $form_state->getValue('nodejs_host'))
      ->set('additional_nodejs_hosts', $form_state->getValue('additional_nodejs_hosts'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
