<?php

namespace Drupal\nodejs_translate\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nodejs_translate\NodejsTranslator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Translate nodes.
 */
class TranslateNodes extends FormBase {

  /**
   * Translator for text service.
   *
   * @var \Drupal\nodejs_translate\NodejsTranslator
   */
  protected $translator;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\nodejs_translate\NodejsTranslator $translator
   *   Translate text.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(NodejsTranslator $translator, EntityTypeManagerInterface $entity_type_manager) {
    $this->translator = $translator;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nodejs_translate.nodejs_translator'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $content = $this->t('<h2>Select content types for translation</h2>');
    $form['node_table'] = [
      '#markup' => $content,
    ];

    $types = [];
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      $types[$contentType->id()] = $contentType->label();
    }

    $form['node_types'] = [
      '#title' => $this->t('Node types'),
      '#type' => 'checkboxes',
      '#options' => $types,
      '#required' => TRUE,
      '#description' => $this->t('Select node types for translation.'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Translate Nodes'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodejs_translate_node_form';
  }

  /**
   * Submit callback.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node_types = $form_state->getValue('node_types');
    $enabled_node_types = [];
    foreach ($node_types as $name => $value) {
      if (!empty($value)) {
        $enabled_node_types[] = $name;
      }
    }

    $nodes = $this->entityTypeManager->getStorage('node')
      ->loadByProperties(['type' => $enabled_node_types]);

    if (empty($nodes)) {
      $this->messenger->addError('Couldn\'t find nodes of selected node types');
    }
    foreach ($nodes as $node) {
      $operations[] = [
        '\Drupal\nodejs_translate\Form\TranslateNodes::translateNodes',
        [$node->id()],
      ];
    }

    $batch = [
      'title' => $this->t('Translating nodes ...'),
      'operations' => $operations,
      'finished' => '\Drupal\nodejs_translate\Form\TranslateNodes::translateNodesFinished',
    ];
    batch_set($batch);
  }

  /**
   * Batch operation.
   */
  public static function translateNodes($row, &$context) {
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($row);

    if (empty($node)) {
      $context['results'][] = 'Created node: ' . $node->id();
      $context['sandbox']['current_id'] = $node->id();
      $context['message'] = 'Created page: ' . $node->label();
      return;
    }

    $translator = \Drupal::service('nodejs_translate.nodejs_translator');
    $translator->translateEntity($node);

    $context['results'][] = 'Created node: ' . $node->id();
    $context['sandbox']['current_id'] = $node->id();
    $context['message'] = 'Created page: ' . $node->label();
  }

  /**
   * Finished callback.
   */
  public static function translateNodesFinished($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One post processed.', '@count posts processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

}
